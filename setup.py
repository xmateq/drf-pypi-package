import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name='drf_is_not_authenticated',
    version='1.0.0',
    description="Django Rest Framework Authentication Class for not authorized users",
    author="Mateusz Krzesiak",
    author_email="mati44491@o2.pl",
    long_description_content_type="text/markdown",
    long_description=long_description,
    packages=setuptools.find_packages(),
    classifiers=(
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU General Public License (GPL)",
        "Operating System :: OS Independent",
        "Development Status :: 5 - Production/Stable",
        "Framework :: Django",
    ),
    install_requires=(
        'djangorestframework',
    ),
    python_requires='>=3.8',
)
