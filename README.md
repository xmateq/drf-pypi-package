# Django Rest Framework Authentication Class for not authorized users

Some DRF views should not be accessible by logged in users.
In such cases we need permission class that will not allow authorized users to access the resource or action.

## Installation
```bash
pip install drf_is_not_authenticated
```

## Use cases
1. Only not registered users should be able to create new user
```python
from drf_is_not_authenticated import IsNotAuthenticated


class RegisterUser(CreateAPIView):
    queryset=User.objects.all()
    permission_classes = (IsNotAuthenticated,)
```

## Contribution
